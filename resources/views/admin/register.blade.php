@extends('layouts.master')


@section('title')
    Regitered Roles | Gestion d'article
@endsection



@section('content')
    
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Registered Roles</h4>
          @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>usertype</th>
                <th>Edit</th>
                <th>DELETE</th>
              </thead>
              <tbody>
                @foreach ($users as $row)
                <tr>
                  <td>{{ $row->id }}</td>
                  <td>{{ $row->name }}</td>
                  <td>{{ $row->phone }}</td>
                  <td>{{ $row->email }}</td>
                  <td>{{ $row->usertype }}</td>

                  <td>
                      <a href="/role-edit/{{ $row->id  }}" class="btn btn-success">Edit</a>
                  </td>
                  <td>
                    <a href="#" class="btn btn-danger">DELETE</a>
                </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card card-plain">
        <div class="card-header">
          <h4 class="card-title"> Table on Plain Background</h4>
          <p class="category"> Here is a subtitle for this table</p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>Name</th>
                <th>Country</th>
                <th>City</th>
                <th>Salary</th>
              </thead>
              <tbody>
                <tr>
                  <td>Dakota Rice</td>
                  <td>Niger</td>
                  <td>Oud-Turnhout </td>
                  <td>$36,738</td>
                </tr>
                <tr>
                  <td>Minerva Hooper</td>
                  <td>Curaçao</td>
                  <td>Sinaai-Waas </td>
                  <td>$23,789</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection


@section('scripts')
    
@endsection