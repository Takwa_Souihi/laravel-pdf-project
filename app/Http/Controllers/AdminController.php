<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
     //
     public function registered()
     {
         //ALLUsers
         $users = User::all();
         return view('admin.register')->with('users',$users);
     }
     public function registeredit(Request $request ,$id)
     {
         $users = User::findOrfail($id);
         return view('admin.register-edit')->with('users',$users);
     }
     public function registerupdate(Request $request ,$id)
     {
         $users = User::find($id);
         $users->name = $request->input('username');
         $users->usertype = $request->input('usertype');
         $users->update();
         return redirect('/role-register')->with('status','Your Data is Updated');
 
     }
}
